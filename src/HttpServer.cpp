#include "HttpServer.h"

ESP8266WebServer *  ESP8266HTTPSERVER;

ESP8266WebServer * initHttpServer(){
    ESP8266HTTPSERVER = new ESP8266WebServer(80);
    ESP8266HTTPSERVER->onNotFound(handleUserRequest);  
    ESP8266HTTPSERVER->begin();
    Serial.println("WEB SERVER is started!");
    return ESP8266HTTPSERVER;
}
void handleUserRequest(){
    String uri = ESP8266HTTPSERVER->uri();
    Serial.print("uri: ");
    Serial.println(uri);

    // 通过handleFileRead函数处处理用户请求资源
    bool fileReadOK = handleFileRead(uri);

    // 如果在SPIFFS无法找到用户访问的资源，则回复404 (Not Found)
    if (!fileReadOK){                                                 
        ESP8266HTTPSERVER->send(404, "text/plain", "404 Not Found, " + uri); 
    }
}

bool handleFileRead(String resource) {            //处理浏览器HTTP访问
    if (resource.endsWith("/")) {                   // 如果访问地址以"/"为结尾
        resource = "/index.html";                     // 则将访问地址修改为/index.html便于SPIFFS访问
    }

    String contentType = getContentType(resource);  // 获取文件类型
    if (SPIFFS.exists(resource)) {                     // 如果访问的文件可以在SPIFFS中找到
        File file = SPIFFS.open(resource, "r");          // 则尝试打开该文件
        ESP8266HTTPSERVER->streamFile(file, contentType);// 并且将该文件返回给浏览器
        file.close();                                // 并且关闭文件
        return true;                                 // 返回true
    }
    return false;                                  // 如果文件未找到，则返回false
}

// 获取文件类型
String getContentType(String filename){
    if(filename.endsWith(".htm")) return "text/html";
    else if(filename.endsWith(".html")) return "text/html";
    else if(filename.endsWith(".css")) return "text/css";
    else if(filename.endsWith(".js")) return "application/javascript";
    else if(filename.endsWith(".png")) return "image/png";
    else if(filename.endsWith(".gif")) return "image/gif";
    else if(filename.endsWith(".jpg")) return "image/jpeg";
    else if(filename.endsWith(".ico")) return "image/x-icon";
    else if(filename.endsWith(".xml")) return "text/xml";
    else if(filename.endsWith(".pdf")) return "application/x-pdf";
    else if(filename.endsWith(".zip")) return "application/x-zip";
    else if(filename.endsWith(".gz")) return "application/x-gzip";
    return "text/plain";
}