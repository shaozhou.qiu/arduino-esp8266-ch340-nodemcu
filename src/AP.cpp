#include "AP.h"

// AP mode
bool ESP8266WiFiAP::start(String pwd) {
  IPAddress ip(10, 0, 0, 1);
  IPAddress gateway(10, 0, 0, 1);
  IPAddress subnet(255, 0, 0, 0);

//  WiFi.mode(WIFI_AP);
  WiFi.enableAP(true);
  
  WiFi.softAPConfig(ip, gateway, subnet);

  String apName = ("ESP8266_" + (String)ESP.getChipId());
  const char * ssid= apName.c_str();
  bool ret = WiFi.softAP(ssid, pwd, 3, 0, 4);  
  // if(!ret) return ret;
  Serial.println("AP Name:");
  Serial.println(ssid); 
  Serial.println("AP Password:");
  Serial.println(pwd); 
  Serial.println("AP Mode:");
  Serial.println(WiFi.getMode());
  Serial.println("AP Status:");
  Serial.println(ret);

  IPAddress myIP = WiFi.softAPIP();
  Serial.println("AP IP Address:");
  Serial.println(myIP);
//  WiFi.disconnect();
  return ret;
}
