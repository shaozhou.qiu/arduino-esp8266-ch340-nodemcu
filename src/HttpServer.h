#ifndef ESP8266AP_HTTPSERVER_H_
#define ESP8266AP_HTTPSERVER_H_


#include <ESP8266WiFi.h>      
#include <ESP8266WiFiMulti.h> 
#include <ESP8266WebServer.h> 


ESP8266WebServer *  initHttpServer();
void handleUserRequest();
bool handleFileRead(String resource);
// 获取文件类型
String getContentType(String filename);
#endif 
	