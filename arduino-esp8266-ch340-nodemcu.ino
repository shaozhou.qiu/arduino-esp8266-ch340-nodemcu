#include "src/AP.h"
#include "src/HttpServer.h"
ESP8266WebServer * httpServer = NULL;
void setup() {
  Serial.begin( 115200 );
  Serial.println("");

  WiFi.enableSTA(true);
  WiFi.begin("BQQ", "shaomingqiu2019");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("IP:");
  Serial.println(WiFi.localIP());

//  ESP8266WiFiAP AP;
//  AP.start("12345678");

  httpServer = initHttpServer();
  
}

void loop() {
   httpServer->handleClient();
}
