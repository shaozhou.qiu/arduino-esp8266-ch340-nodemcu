
(function(global,  factory){
    typeof(ETSD)=='undefined' && factory(global);
})(window,  function(win){
    'use strict';
    var doc=document,ELCLS=function(el){ if(el){var o=new ELCLS;for(var k in o){ el[k]=o[k];}} return el; };
    ELCLS.prototype.ce = function(tag, attrs){ return this._ce(null, tag, attrs); }
    ELCLS.prototype.ceNS = function(tag, attrs){ return this._ce("http://www.w3.org/2000/svg", tag, attrs); }
    ELCLS.prototype._ce = function(ns, tag, attrs){
        var el = (ns!=null)?doc.createElementNS(ns, tag):doc.createElement(tag);
        ELCLS(el); el.ca(attrs); this instanceof Element && this.appendChild(el);
        return el;
    };
    ELCLS.prototype.ca = function(attrs){
        if(!attrs)return;
        var ie= this instanceof Element, d=[];
        for(var k in attrs) if(ie){
            if(typeof(attrs[k]) == 'function'){this[k] = attrs[k];continue; }
            else if(typeof(attrs[k]) == 'object'){this[k] = attrs[k];continue; }
            this.setAttribute(k, attrs[k]);
        }
    }
    var EL = new ELCLS;
    var ETCLS=function(){
        for(var k in EL)this[k] = EL[k];
        this['thread']=Thread;
        this['floatMenu']=FloatMenu;
        this['messagebox']=new Messagebox;
    };
    ETCLS.prototype.iniEL = function(el){
        (el instanceof Element) && (ELCLS(el));
        return el;
    }
    var Messagebox = function(){}
    Messagebox.prototype.show = function(content, callback){
        var m = EL.ce('div', {"style":"position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; background: #000; opacity: 0.3;z-index: "+document.all.length});
        doc.body.appendChild(m);
        var b = EL.ce('div', {"style":"position: fixed; top: 25%; left: 25%; width: 50%; height: 50%; background: #fff; border: 1px solid #aaa; z-index: "+document.all.length});
        doc.body.appendChild(b);
        (b.ce('div', {"style":"padding: 5px;border-bottom: 1px solid #aaa; background: #f3f3f3; "})).innerText='ET';
        var c = b.ce('div', {"style":"padding: 5px; "});
        if(typeof(content)=='object') {c.appendChild(content);}else{c.innerHTML=content;}
        (b.ce('div', {"style":"bottom: 0px;position: absolute;text-align: center;width: 100%;padding: 5px;"})).
        ce("input", {'type':'button', 'value':' ok ', "style":"border: 1px solid #888;", onclick:function(){
            callback.call(b); m.remove(); b.remove();
        }});
    }
    var Thread=function(url, callback){
        var el = EL.ce("iframe", {"style":"position: fixed;left: 0;bottom: 0;visibility: hidden;width: 1px;height: 1px;"});
        el.onload=function(){callback.call(this); this.remove(); }
        el.src = url;
        doc.body.appendChild(el);
    }
    var FloatMenu=function(menu){
        var el = EL.ce("div", {"style":"position: fixed; left: 10px; bottom: 10px; "})
        ,m = el.ce("div", {"style":"border: 1px solid #ccc; padding: 5px; "})
        ,a = el.ce("a", {"href":"javascript:void(0)","style":"width: 15px; height: 15px; border-radius: 25px; border: 5px solid #087bea;display: block;"});
        a.onclick=function(){
            if(m.innerText != ''){m.innerText='';return;}
            for(var k in menu){var p =m.ce('p'); (p.ce('a', {onclick:menu[k]})).innerHTML=k; }
        }
        doc.body.appendChild(el);
    };
    win.ET = new ETCLS;
});