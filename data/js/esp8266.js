(function($, Vue, factry){
	if(!$ || !Vue)return;
	factry($, Vue);
})(jQuery, Vue,function($, Vue){

	new Vue({
		el : '#esp8266'
		,data:{ssids:[]}
		,created:function(){
			var _me = this;
			$.ajax({url:'/ssids', success:function(e){ _me.ssids = e; }});
		}
		,methods:{
			del:function(n){this.ssids.splice(n,1); }
			,add:function(){
				var _me = this;
				this.scan(function(ssid, pwd){
					if(!ssid)return;
					_me.ssids.push({ssid:ssid, pwd:pwd}); 

				});
			}
			,scan:function(callback){
				$.ajax({
					// url:'/scan'
					url:'/'
					,'type':'get'
					,'success':function(e){
						e=[{ssid:'BQQ'}, {ssid:'BQQ-5G'}];
						for(var i=0; i<100; i++)e.push({ssid:'s'+i});
						var el = ET.ce('div',{"style":"overflow: auto; height: 350px; "}),ul= el.ce('ul');
						for(var i=0; i<e.length; i++) (ul.ce('li', {
							onclick:function(){
								$(this).parent().find('>').removeClass('selected')
								$(this).addClass('selected');
							}
						})).innerHTML=e[i].ssid;
						ET.messagebox.show(el, function(){
							var ssid=$(this).find('.selected').html();
							if(!ssid)return;
							var pwd = prompt(ssid+'\' password:');
							callback(ssid, pwd);
						});
					}
				});
			}
			,clkPin:function(){
				var el = $(this.$el).find('.esp8266chart')[0];
				var pin = el.value;
				this.pinset(Object.keys(pin[0])[0]);
			}
			,pinset:function(pin, v){
				$.ajax({
					// url:'/pinset'
					url:'/'
					,data:{pin:pin}
					,'type':'get'
					,'success':function(e){
						e={errcode:0, pin:pin,data:0};
						if(e.errcode!=0){alert('error:'+e.errcode);return;}
						alert("Success, current "+e.pin+" is "+e.data);
						
					}
				});
			}
		}
	});

	
	var esp8266chart = function(el){
		this.el = ET.iniEL(el);
		this.init(this.el);
	}
	esp8266chart.prototype.init = function(el){
		var hsvg = (el.tagName == 'SVG' ||el.tagName == 'svg')?el:el.ceNS('svg', {id:'hsvg'});
		$(hsvg).css({
			'width': $(el).width()
			// ,'height': $(el).height()
			,'height': 600
		});
		var pins = [
			[{'A0':''}, {'ADC0':'ab36fe'}, {'TOUT':'ffe171'}]
			, [{'GND':''}, {'GND':'000000'}]
			, [{'VU':''}, {'VUSB':'fd7e78'}, {'USB power output':''}]
			, [{'SD3':''}, {'GPIO10':''}, {'SDD3':'fe7000'}]
			, [{'SD2':''}, {'GPIO9':''}, {'SDD2':'fe7000'}]
			, [{'SD1':''}, {'MOSI':'green'}, {'SDD1':'fe7000'}]
			, [{'CMD':''}, {'cs':'green'}, {'SDCMD':'fe7000'}]
			, [{'SD0':''}, {'MISO':'green'}, {'SDD0':'fe7000'}]
			, [{'CLK':''}, {'SCLK':'green'}, {'SDCLK':'fe7000'}]
			, [{'GND':''}, {'GND':'000000'}]
			, [{'3V3':''}, {'3.3V':'ff0000'}]
			, [{'EN':''}]
			, [{'RST':''}, {'RST':'ffe171'}]
			, [{'GND':''}, {'GND':'000000'}]
			, [{'Win':''}, {'WIN 5V':'yellow'}]
			, [{'3V3':''}, {'3.3V':'ff0000'}]
			, [{'GND':''}, {'GND':'000000'}]
			, [{'D10':''}, {'GPIO1':''}, {'TXD0':'c8f4ff'}]
			, [{'D9':''}, {'GPIO3':''}, {'RXD0':'c8f4ff'}]
			, [{'D8':''}, {'GPIO15':''}, {'TXD2':'c8f4ff'}]
			, [{'D7':''}, {'GPIO13':''}, {'RXD2':'c8f4ff'}]
			, [{'D6':''}, {'GPIO12':''}]
			, [{'D5':''}, {'GPIO14':''}]
			, [{'3V3':''}, {'3.3V':'ff0000'}]
			, [{'GND':''}, {'GND':'000000'}]
			, [{'D4':''}, {'GPIO2':''}, {'TXD1':'c8f4ff'}]
			, [{'D3':''}, {'GPIO0':''}, {'FLASH':'green'}]
			, [{'D2':''}, {'GPIO4':''}]
			, [{'D1':''}, {'GPIO5':''}]
			, [{'D0':''}, {'GPIO16':''}, {'USER':'green'}]
		];
		Object.defineProperty(this, 'pins', {
			get:function(){return pins;}
		});
		this.inibackground(hsvg, pins);
	}
	esp8266chart.prototype.inibackground = function(svg, pins){
		var w = $(svg).width()*0.4, h = $(svg).height()
		,x = $(svg).width()*0.1+w/2, y = 0, g = svg.ceNS('g');
		var p = [
			[x+w*0.05, y]
			, [x, y+h*0.02]
			, [x, h]
			, [x+w, h]
			, [x+w, y+h*0.02]
			, [x+w-w*0.05, y]
		], points='';
		for (var i = 0; i <p.length; i++) {
			points += p[i][0]+','+p[i][1]+' ';
		}
		g.innerHTML = 'not support!';
		var plg = g.ceNS('polygon', {
			points:points
			, style:"fill:#116c8b;stroke:#888;stroke-width:1"
			, onclick:function(){
				if ( event && event.stopPropagation )
					event.stopPropagation();
				else
					window.event.cancelBubble = true;
			}
		});
		var txt = g.ceNS('text', {x:p[1][0]+w*0.8/3, y:p[1][1]+13, style:"fill:#fff;"});
		txt.innerHTML = "DEVKIT"

		var l = pins.length/2;
		var sp = h/l, _me = this;
		for (var i = 0; i < pins.length; i++) {
			var pg = g.ceNS('g',{pin:pins[i], onclick:function(){
				_me.el.value=_me.el.data=this.pin; 
			}}), cx=0, cy=0;
			if(i>=l){
				cx = p[5][0];
				cy = p[3][1]-sp*(i-l);
			}else{
				cx = p[0][0];
				cy = p[1][1]+sp*(i)+20;
			}
			var pnt = pg.ceNS('ellipse', {cx:cx, cy:cy, rx:5, ry:5, 'style':'fill:#fff'});
			var tg = null;
			for(var j=0; j<pins[i].length;j++){
				var x = (i>=l)?cx+20+80*j:cx-70-80*j;
				var pi = pins[i][j];
				tg = pg.ceNS('g',{'value':pi});
				var c = pi[Object.keys(pi)[0]];
				c=c?c:'ffffff';
				tg.ceNS('rect', {x:x-10, y:cy-16, width:j?70:50, height:20, stroke:'#666', fill:'#'+c});
				(tg.ceNS('text', {x:x, y:cy, style:"fill:#"+(0xffffff^("0x"+c)).toString(16)+";"})).innerHTML = Object.keys(pi)[0];
				// break;
			}

			
		}

		return {x:x, y:y, w:w, h:h};
	}

	$('.esp8266chart').each(function(){ 
		window.esp8266=new esp8266chart(this); 
	});

});